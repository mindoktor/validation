package validation

import "regexp"

// SimpleValidator implements very simple utility routines for
// validation of data.
type SimpleValidator struct {
	HasErrors bool
}

// IsZero validates that the field is equal to the zero value of the type. Allowed inputs are int,
// int64, string and bool all other types of input will fail validation.
func (v *SimpleValidator) IsZero(field interface{}) {
	if v.HasErrors {
		return
	}

	switch field.(type) {
	default:
		v.HasErrors = true
	case bool:
		if field.(bool) {
			v.HasErrors = true
		}
	case int:
		if field.(int) != 0 {
			v.HasErrors = true
		}
	case int64:
		if field.(int64) != 0 {
			v.HasErrors = true
		}
	case string:
		if field.(string) != "" {
			v.HasErrors = true
		}
	}
}

// Min validates that the field is greater than min. Allowed inputs are int and
// int64, all other types of input will fail validation.
func (v *SimpleValidator) Min(field interface{}, min int) {
	if v.HasErrors {
		return
	}

	switch field.(type) {
	default:
		v.HasErrors = true
	case int:
		if field.(int) < min {
			v.HasErrors = true
		}
	case int64:
		if field.(int64) < int64(min) {
			v.HasErrors = true
		}
	}
}

// Max validates that the field is less than max.
func (v *SimpleValidator) Max(field interface{}, max int) {
	if v.HasErrors {
		return
	}

	switch field.(type) {
	default:
		v.HasErrors = true
	case int:
		if field.(int) > max {
			v.HasErrors = true
		}
	case int64:
		if field.(int64) > int64(max) {
			v.HasErrors = true
		}
	}
}

// MinFloat validates that the field is greater than min.
func (v *SimpleValidator) MinFloat(field float64, min float64) {
	if v.HasErrors {
		return
	}

	if field < min {
		v.HasErrors = true
	}
}

// MaxFloat validates that the field is less than max.
func (v *SimpleValidator) MaxFloat(field float64, max float64) {
	if v.HasErrors {
		return
	}

	if field > max {
		v.HasErrors = true
	}
}

// Range validates that the value is greater than or equal to min and smaller
// that or equal to max.
func (v *SimpleValidator) Range(field int, min, max int) {
	if v.HasErrors {
		return
	}

	if field < min || field > max {
		v.HasErrors = true
	}
}

// LengthMin validates that the field is greater than min.
func (v *SimpleValidator) LengthMin(field string, min int) {
	if v.HasErrors {
		return
	}

	if len(field) < min {
		v.HasErrors = true
	}
}

// LengthMax validates that the field is less than max.
func (v *SimpleValidator) LengthMax(field string, max int) {
	if v.HasErrors {
		return
	}

	if len(field) > max {
		v.HasErrors = true
	}
}

// LengthRange validates that the field is longer than min.
func (v *SimpleValidator) LengthRange(field []string, min, max int) {
	if v.HasErrors {
		return
	}

	if len(field) < min || len(field) > max {
		v.HasErrors = true
	}
}

// Required is a shorthand for LengthMin(xyz, 1)
func (v *SimpleValidator) Required(field string) {
	v.LengthMin(field, 1)
}

// ContainsOnly checks so that the field slice only contains the allowed values
// listed in the allowed slice.
func (v *SimpleValidator) ContainsOnly(field, allowed []string) {
	if v.HasErrors {
		return
	}

	for _, value := range field {
		var valid bool
		for _, validValue := range allowed {
			if value == validValue {
				valid = true
			}
		}
		if !valid {
			v.HasErrors = true
			return
		}
	}
}

const (
	// RegExEmail matches email addresses (taken from revel's validation package)
	RegExEmail = "^[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[a-zA-Z0-9](?:[\\w-]*[\\w])?$"

	// RegExSWMobile matches a swedish mobilephone number
	RegExSWMobile = `^\s*(0|(\+46)|(0046))\s*(70|72|73|76)\d*\s*-?\s*(?:\d ?){4,}\s*$`
)

// RegEx checks that the given field conforms to the format
// of the provided regex
func (v *SimpleValidator) RegEx(field, regex string) {
	if v.HasErrors {
		return
	}

	r, err := regexp.Compile(regex)
	if err != nil {
		v.HasErrors = true
		return
	}

	if !r.MatchString(field) {
		v.HasErrors = true
	}
}

// Email validates field against RegExEmail
func (v *SimpleValidator) Email(field string) {
	v.RegEx(field, RegExEmail)
}

// SWMobile validates field against RegExSWMobile
func (v *SimpleValidator) SWMobile(field string) {
	v.RegEx(field, RegExSWMobile)
}
