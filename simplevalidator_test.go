package validation

import (
	"encoding/json"
	"testing"
)

func TestIsZero(t *testing.T) {
	cases := []struct {
		in   interface{}
		want bool
	}{
		{int(0), false},
		{int(1), true},
		{int64(0), false},
		{int64(1), true},
		{"", false},
		{"foo", true},
	}
	for _, c := range cases {
		var v SimpleValidator
		v.IsZero(c.in)
		if v.HasErrors != c.want {
			t.Errorf("IsZero(%d) == %t, want %t", c.in, v.HasErrors, c.want)
		}
	}
}

func TestMin(t *testing.T) {
	cases := []struct {
		in   interface{}
		min  int
		want bool
	}{
		{int(1), 0, false},
		{int(1), 2, true},
		{int64(1), 1, false},
		{int64(1), 2, true},
		{"foo", 2, true},
	}
	for _, c := range cases {
		var v SimpleValidator
		v.Min(c.in, c.min)
		if v.HasErrors != c.want {
			t.Errorf("Min(%d, %d) == %t, want %t", c.in, c.min, v.HasErrors, c.want)
		}
	}
}

func TestMax(t *testing.T) {
	cases := []struct {
		in   interface{}
		max  int
		want bool
	}{
		{int(1), 0, true},
		{int(1), 2, false},
		{int64(1), 1, false},
		{int64(2), 1, true},
		{"foo", 2, true},
	}
	for _, c := range cases {
		var v SimpleValidator
		v.Max(c.in, c.max)
		if v.HasErrors != c.want {
			t.Errorf("Max(%d, %d) == %t, want %t", c.in, c.max, v.HasErrors, c.want)
		}
	}
}

func TestMinFloat(t *testing.T) {
	cases := []struct {
		in, min float64
		want    bool
	}{
		{0.5, 1.5, true},
		{2.5, 1.5, false},
		{1.5, 1.5, false},
	}
	for _, c := range cases {
		var v SimpleValidator
		v.MinFloat(c.in, c.min)
		if v.HasErrors != c.want {
			t.Errorf("MinFloat(%f, %f) == %t, want %t", c.in, c.min, v.HasErrors, c.want)
		}
	}
}

func TestMaxFloat(t *testing.T) {
	cases := []struct {
		in, max float64
		want    bool
	}{
		{0.5, 1.5, false},
		{2.5, 1.5, true},
		{1.5, 1.5, false},
	}
	for _, c := range cases {
		var v SimpleValidator
		v.MaxFloat(c.in, c.max)
		if v.HasErrors != c.want {
			t.Errorf("MaxFloat(%f, %f) == %t, want %t", c.in, c.max, v.HasErrors, c.want)
		}
	}
}

func TestRange(t *testing.T) {
	cases := []struct {
		in, min, max int
		want         bool
	}{
		{1, 1, 2, false},
		{0, 1, 2, true},
		{3, 1, 2, true},
	}
	for _, c := range cases {
		var v SimpleValidator
		v.Range(c.in, c.min, c.max)
		if v.HasErrors != c.want {
			t.Errorf("Range(%d, %d, %d) == %t, want %t", c.in, c.min, c.max, v.HasErrors, c.want)
		}
	}
}

func TestLengthMin(t *testing.T) {
	cases := []struct {
		in   string
		min  int
		want bool
	}{
		{"", 1, true},
		{"foo", 1, false},
		{"foo", 4, true},
	}
	for _, c := range cases {
		var v SimpleValidator
		v.LengthMin(c.in, c.min)
		if v.HasErrors != c.want {
			t.Errorf("LengthMin(%q, %d) == %t, want %t", c.in, c.min, v.HasErrors, c.want)
		}
	}
}

func TestLengthMax(t *testing.T) {
	cases := []struct {
		in   string
		max  int
		want bool
	}{
		{"", 1, false},
		{"foo", 1, true},
		{"foo", 4, false},
	}
	for _, c := range cases {
		var v SimpleValidator
		v.LengthMax(c.in, c.max)
		if v.HasErrors != c.want {
			t.Errorf("LengthMax(%q, %d) == %t, want %t", c.in, c.max, v.HasErrors, c.want)
		}
	}
}

func TestLengthRange(t *testing.T) {
	cases := []struct {
		in       []string
		min, max int
		want     bool
	}{
		{[]string{"foo", "bar"}, 0, 3, false},
		{[]string{"foo", "bar"}, 0, 1, true},
		{[]string{"foo"}, 2, 3, true},
		{[]string{}, 1, 2, true},
	}
	for _, c := range cases {
		var v SimpleValidator
		v.LengthRange(c.in, c.min, c.max)
		if v.HasErrors != c.want {
			t.Errorf("LengthRange(%q, %d, %d) == %t, want %t", c.in, c.min, c.max, v.HasErrors, c.want)
		}
	}
}

func TestContainsOnly(t *testing.T) {
	cases := []struct {
		in, allowed []string
		want        bool
	}{
		{[]string{"foo", "bar"}, []string{"foo", "bar"}, false},
		{[]string{"foo", "bar"}, []string{"bar"}, true},
		{[]string{"foo", "bar"}, []string{"baz"}, true},
	}
	for _, c := range cases {
		var v SimpleValidator
		v.ContainsOnly(c.in, c.allowed)
		if v.HasErrors != c.want {
			t.Errorf("ContainsOnly(%q, %q) == %t, want %t", c.in, c.allowed, v.HasErrors, c.want)
		}
	}
}

type guideHeadacheData struct {
	// cases.Case
	DebutDate    string
	PreviousDiag string
	Message      string

	// cases.DetailsHeadache
	ID                          int64
	CaseID                      int64
	SeriousAndUrgent            int
	Fever                       int
	Vomit                       int
	IrregularOrRegular          int
	HowOften                    int
	GroupedHeadachesDescription string
	CertainTimeOfDayDescription string
	LengthOfHeadache            int
	WorseRecently               int
	AcheLocation                int
	ChewingCheekMusclesTemples  string
	AchePremonition             string
	VisionChange                int
	PreviouslyVisitedDoctor     int
	PreviousExaminations        string

	ChewingCheekMusclesTemp  []string
	PreviousExaminationsTemp []string
}

func BenchmarkSimpleValidator(b *testing.B) {
	headacheJSON := []byte(`{"ChewingCheekMusclesTemp":["tendercheekmuscles","tendertemples"],"PreviousExaminationsTemp":["doctorexamination"],"SeriousAndUrgent":2,"Fever":2,"Vomit":2,"DebutDate":"now","IrregularOrRegular":2,"HowOften":2,"GroupedHeadachesDescription":"pattern","CertainTimeOfDayDescription":"certain","LengthOfHeadache":2,"WorseRecently":2,"AcheLocation":1,"AchePremonition":"advance","VisionChange":2,"PreviouslyVisitedDoctor":2,"PreviousDiag":"no diag","Message":"other medical"}`)

	var guide guideHeadacheData
	if err := json.Unmarshal(headacheJSON, &guide); err != nil {
		b.Fatal("json unmarshal failed:", err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		var v SimpleValidator

		// Step 1
		v.Range(guide.SeriousAndUrgent, 1, 2)
		v.Range(guide.Fever, 1, 2)
		v.Range(guide.Vomit, 1, 2)

		// Step 3
		v.LengthMin(guide.DebutDate, 1)

		// Step 4
		v.Range(guide.IrregularOrRegular, 1, 2)

		if guide.IrregularOrRegular != 1 {
			v.Range(guide.HowOften, 1, 4)

			v.LengthMin(guide.GroupedHeadachesDescription, 1)
		}

		// Step 5
		v.LengthMin(guide.CertainTimeOfDayDescription, 1)
		v.Range(guide.LengthOfHeadache, 1, 5)
		v.Range(guide.WorseRecently, 1, 2)

		// Step 6
		v.Range(guide.AcheLocation, 1, 3)
		v.LengthRange(guide.ChewingCheekMusclesTemp, 0, 3)
		v.ContainsOnly(guide.ChewingCheekMusclesTemp, []string{
			"worsewhenchewing",
			"tendercheekmuscles",
			"tendertemples",
		})

		// No validation to be done here
		v.LengthMin(guide.AchePremonition, 1)
		v.Range(guide.VisionChange, 1, 2)

		// Step 7
		v.Range(guide.PreviouslyVisitedDoctor, 1, 3)

		if guide.PreviouslyVisitedDoctor != 3 {
			v.LengthMin(guide.PreviousDiag, 1)

			v.LengthRange(guide.PreviousExaminationsTemp, 0, 2)
			v.ContainsOnly(guide.PreviousExaminationsTemp, []string{
				"doctorexamination",
				"xraybrain",
			})
		}

		if v.HasErrors {
			b.Error("HasErrors == true, want false")
		}
	}
}

type TT struct {
	val         string
	shouldError bool
}

func TestEmail(t *testing.T) {

	tt := []TT{
		TT{"test123@example.com", false},
		TT{"foo_bar@example.com", false},
		TT{"foo.bar@example.com", false},
		TT{"foo$bar@example.com", false},
		TT{"@example.com", true},
		TT{"test123", true},
		TT{"test123@", true},
	}

	for i, tc := range tt {
		v := &SimpleValidator{}
		v.Email(tc.val)
		if v.HasErrors != tc.shouldError {
			t.Errorf("%d/%d: %s HasErrors(%t), expected(%t)", i, len(tt), tc.val, v.HasErrors, tc.shouldError)
		}
	}
}

func TestSWMobile(t *testing.T) {
	tt := []TT{
		TT{"0706789", false},
		TT{"072-6789", false},
		TT{"073-6 7 89", false},
		TT{"076 67 89", false},
		TT{"0760 -67 89", false},

		TT{"071 67 89", true},
		TT{"062-6789", true},
		TT{"073 6-7 89", true},
	}

	for i, tc := range tt {
		v := &SimpleValidator{}
		v.SWMobile(tc.val)
		if v.HasErrors != tc.shouldError {
			t.Errorf("%d/%d: %s HasErrors(%t), expected(%t)", i, len(tt), tc.val, v.HasErrors, tc.shouldError)
		}
	}
}
